#include <stdio.h>

const float PI = 3.1415927;
const char angels[15] = "blue angels";

int main() {
    // PI = PI + 1.0; can't do this to a const
    // angels[3] = 'u'; this either
    puts(angels);
    printf("%f\n", PI);
}
