#include <stdio.h>
#include <string.h>

char name[4];

int main() {
    strcpy(name, "Sam"); /* use strcpy to copy a string constant */
    /* name = "Sam"
     * syntax not permitted except at declaration
     */
    return 0;
}
