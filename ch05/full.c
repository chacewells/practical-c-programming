#include <stdio.h>
#include <string.h>

char first[100]; /* user's first name */
char last[100];  /* user's last name */

/* user's first and last name */
char full[200];

int main() {
    printf("Enter first name: ");
    fgets(first, sizeof(first), stdin);
    /* trim the newline */
    first[strlen(first)-1] = '\0';

    printf("Enter last name: ");
    fgets(last, sizeof(last), stdin);
    /* trim the newline */
    last[strlen(last)-1] = '\0';

    strcpy(full, first);
    strcat(full, " ");
    strcat(full, last);

    printf("The name is %s\n", full);
    return  0;
}
