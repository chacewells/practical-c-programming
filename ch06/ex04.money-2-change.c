#include <stdio.h>
#include <stdlib.h>

typedef unsigned short cent;
typedef unsigned short tally;

/* constants for coin values */
const cent QUARTER_VALUE = 25;
const cent DIME_VALUE =    10;
const cent NICKEL_VALUE =   5;
const cent PENNY_VALUE =    1;

/* organizer for coins */
struct CoinBag {
    tally quarters;
    tally dimes;
    tally nickels;
    tally pennies;
} coin_bag;

/* the user's money, < $1.00 */
cent money_amount;

/* user input */
char line[100];

void tally_coins(cent money_amount, struct CoinBag *bag);

int main() {
    printf("Enter a value in cents: ");
    
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%hu", &money_amount);

    tally_coins(money_amount, &coin_bag);

    printf("%hu quarters, %hu dimes, %hu nickels, %hu pennies\n",
        coin_bag.quarters,
        coin_bag.dimes,
        coin_bag.nickels,
        coin_bag.pennies);
    return 0;
}

void tally_coins(cent money_amount, struct CoinBag *bag) {
    /* for quarters, dimes, and nickels:
       the bag gets money_amount / that coin's value;
       then update money_amount %= coin type;
       finally, all that's left is pennies */
    bag->quarters = money_amount / QUARTER_VALUE;
    money_amount %= QUARTER_VALUE;

    bag->dimes = money_amount / DIME_VALUE;
    money_amount %= DIME_VALUE;

    bag->nickels = money_amount / NICKEL_VALUE;
    money_amount %= NICKEL_VALUE;

    bag->pennies = money_amount;
}
