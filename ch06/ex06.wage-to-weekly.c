#include <stdio.h>

/* more than 40 hours gets time and a half */
const int FULL_WEEK = 40;
const float OVERTIME_PAY = 1.5;

/* hours worked this week */
int hours_worked;
/* employee's wage */
float wage;
/* employee's total pay this week */
float total_weekly_pay;

/* user input */
char line[100];

float compute_overtime_pay(int hours_worked, float wage);
float compute_regular_pay(int hours_worked, float wage);
float compute_weekly_pay(int hours_worked, float wage);

int main() {
    /* get the wage */
    printf("Enter the wage: ");
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%f", &wage);

    /* get the hours worked */
    printf("Enter the hours worked: ");
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &hours_worked);
    
    /* compute the weekly pay and report it */
    total_weekly_pay = compute_weekly_pay(hours_worked, wage);
    printf("Total weekly pay: $%.2f\n", total_weekly_pay);

    return 0;
}

float compute_overtime_pay(int hours_worked, float wage) {
    float result;
    int overtime_hours = hours_worked - FULL_WEEK;
    if (overtime_hours <= 0) {
        result = 0.0;
    } else {
        result = ( ((float)overtime_hours) * wage ) * OVERTIME_PAY;
    }

    return result;
}

float compute_regular_pay(int hours_worked, float wage) {
    float result;
    int regular_hours;
    if (hours_worked <= FULL_WEEK) {
        regular_hours = hours_worked;
    } else {
        regular_hours = FULL_WEEK;
    }
    result = ((float)regular_hours) * wage;
    
    return result;
}

float compute_weekly_pay(int hours_worked, float wage) {
    float result = compute_regular_pay(hours_worked, wage);
    result += compute_overtime_pay(hours_worked, wage);

    return result;
}
