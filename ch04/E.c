/*
 * E.c
 * Aaron Wells
 * Prints the letter "E" with asterisks
 * on seven lines
 */
#include <stdio.h>
#define E_LENGTH 7
#define E_WIDTH  6

/* storing e as a 7x6 matrix */
/* each row is 5 characters plus the null \0 */
char E[E_LENGTH][E_WIDTH] = {
    "*****",
    "*",
    "*",
    "*****",
    "*",
    "*",
    "*****"
};

int main() {
    for (int i = 0; i < E_LENGTH; ++i) {
        printf("%s\n", E[i]);
    }
}
