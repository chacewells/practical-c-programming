/* HELLO.c
 * prints HELLO in big block letters
 * letter height: 7 characters
 * letter width: 5 characters
 */
#include <stdio.h>

/* defined constants for letters dimensions */
#define LETTER_HEIGHT 7
#define COLUMN_WIDTH  6

/*
 * each row is a line of the word "HELLO"
 * each letter is 7 high x 5 wide
 */

int main() {
    puts("H   H EEEEE L     L      OOO ");
    puts("H   H E     L     L     O   O");
    puts("H   H E     L     L     O   O");
    puts("HHHHH EEEEE L     L     O   O");
    puts("H   H E     L     L     O   O");
    puts("H   H E     L     L     O   O");
    puts("H   H EEEEE LLLLL LLLLL  OOO ");

    return 0;
}
