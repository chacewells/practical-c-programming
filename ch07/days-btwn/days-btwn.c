#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// month number constants
const int
    JAN =  0,
    FEB =  1,
    MAR =  2,
    APR =  3,
    MAY =  4,
    JUN =  5,
    JUL =  6,
    AUG =  7,
    SEP =  8,
    OCT = 9,
    NOV = 10,
    DEC = 11;

// days in month validation constants
const int
    LONG_MONTH_MAX_DAYS     = 31,
    SHORT_MONTH_MAX_DAYS    = 30,
    FEB_MAX_DAYS_NO_LEAP    = 28,
    FEB_MAX_DAYS_LEAP       = 29;

// sprintf date format to parse FROM clean string
const char *DATE_FORMAT_STRPTIME = "%m/%d/%Y"; // mmddyyyy

// user input
char line[100];

// validated date strings
char clean_date_a[30];
char clean_date_b[30];

// buckets for differencing and date munging
time_t time_a, time_b, difference_t;
struct tm time_struct_a, time_struct_b;
double difference_seconds;
int difference_days;

void init();

void validate_date(struct tm user_date);
void validate_month(int month);
void validate_day_of_month(struct tm user_date);
int is_leap_year(int year);

int main(int argc, char *argv[]) {
    init();
    if (argc != 3) {
        printf("usage: %s mm/dd/yyyy mm/dd/yyyy\n", argv[0]);
        exit(1);
    }

    validate_date(time_struct_a);
    validate_date(time_struct_b);

    // puts("parsed numbers");

    /* convert to time_t */
    strptime(argv[1], DATE_FORMAT_STRPTIME, &time_struct_a);
    time_a = mktime(&time_struct_a);

    strptime(argv[2], DATE_FORMAT_STRPTIME, &time_struct_b);
    time_b = mktime(&time_struct_b);

    /* get difference */
    difference_seconds = difftime(time_b, time_a);

    /* convert difference to days */
    difference_days = difference_seconds / (int)(3600 * 24);

    /* print result */
    printf("%d days\n", difference_days);
    
    return 0;
}

void init() {
}

void validate_date( struct tm user_date ) {
    validate_month(user_date.tm_mon);
    validate_day_of_month(user_date);
}

void validate_month(int month) {
    /* valid month falls between 1 and 12 */
    if ( month < JAN || month > DEC ) {
        printf( "%d is not a valid month. Month must be between %d and %d\n",
            month, JAN, DEC );
        exit(1);
    }
}

void validate_day_of_month( struct tm user_date ) {
    int valid = 1;
    char error_message[100];
    int max_days;

    /* select max days to validate against */
    switch (user_date.tm_mon) {
        case FEB:
            max_days = ( is_leap_year(1900 + user_date.tm_year) )
                ? FEB_MAX_DAYS_LEAP
                : FEB_MAX_DAYS_NO_LEAP;
            break;
        case APR:
        case JUN:
        case SEP:
        case NOV:
            max_days = SHORT_MONTH_MAX_DAYS;
            break;
        default:
            max_days = LONG_MONTH_MAX_DAYS;
    }

    if ( user_date.tm_mday < 1 ) { /* too low */
        valid = 0;
        sprintf(error_message, "Day cannot be less than 1. You gave: %d",
            user_date.tm_mday);
    } else if ( user_date.tm_mday > max_days ) { /* too high */
        valid = 0;
        sprintf(
            error_message,
            "Day cannot be more than %d when the month is %d and the year is %d. You gave: %d",
            max_days, 1+user_date.tm_mon, 1900 + user_date.tm_year, user_date.tm_mday);
    }

    if ( !valid ) {
        puts(error_message);
        exit(1);
    }
    /* validation passed */
}

int is_leap_year(int year) {
    if (
        (year % 100 == 0) // not a leap year if divisible by 100 and not by 400
     && (year % 400 != 0)
    ) {
        return 0;
     } else { // otherwise a leap year if divisible by 4
        return year % 4 == 0;
     }
}
