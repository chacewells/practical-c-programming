#include <stdio.h>

const float C_TO_F_SCALE = 1.8;
const float C_TO_F_DIFF  = 32.0;
float c_to_f(float c);
char line[100];
float temp_celcius;
float temp_fahrenheit;

int main() {
    printf("Enter a temperature in Celcius: ");

    /* get the user input and convert it to a number */
    fgets(line, sizeof(line), stdin);
    int filled = sscanf(line, "%f", &temp_celcius);

    temp_fahrenheit = c_to_f(temp_celcius);
    printf("%f degrees celcius is %f degrees fahrenheit\n", temp_celcius, temp_fahrenheit);

    return 0;
}

float c_to_f(float c) {
    return (c * C_TO_F_SCALE) + C_TO_F_DIFF;
}
