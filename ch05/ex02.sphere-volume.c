#include <stdio.h>
#include <math.h>

const double PI = 3.14159265359;

char line[100];
double radius;
double volume;

double sphere_volume(double radius);

int main() {
    printf("Enter the radius: ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%lf", &radius);

    volume = sphere_volume(radius);
    printf("the volume of a sphere with %lf radius is %lf\n", radius, volume);

    return 0;
}

double sphere_volume(double radius) {
    return (4.0/3.0) * PI * pow(radius, 3.0);
}

