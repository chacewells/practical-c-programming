#!/bin/bash
compile()
{
    local src
    local program
    local bin_path
    while [[ "x$1" != x ]];do
        src="$1"; shift
        if [[ "${src:(-2)}" != .c ]];then
            echo "'$src' is not a c source file"
            return 1
        fi

        program="${src%.c}"
        bin_path="$(dirname "$program")/bin"
        [[ -d "$bin_path" ]] || mkdir "$bin_path"
        cc -o "$bin_path/$program" "$src" \
            && echo "$src -> $bin_path/$program" \
            || return 1
    done
}
