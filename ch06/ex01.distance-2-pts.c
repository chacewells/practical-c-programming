#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* data structure for a point */
struct point {
    float x;
    float y;
};

/* coordinates from which the distance will be computed */
struct point *P;
struct point *Q;

/* the distance between the two */
float distance;

/* calculate the cartesian distance as sqrt( (x1-x2)**2, (y1-y2)**2 ) */
float cartesian_distance(struct point *a, struct point *b);
struct point *str_to_point(char *str);

/* user input */
char line[100];

int main() {
    /* P coordinates */
    printf("Enter coordinats for point P (x,y): ");
    fgets(line, sizeof(line), stdin);
    P = str_to_point(line);

    /* Q coordinates */
    printf("Enter the coordinates for point Q (x,y): ");
    fgets(line, sizeof(line), stdin);

    Q = str_to_point(line);
    distance = cartesian_distance(P, Q);

    printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is: %.2f\n",
        P->x, P->y,
        Q->x, Q->y,
        distance);
}

float cartesian_distance(struct point *a, struct point *b) {
    float x_dist = a->x - b->x;
    float y_dist = a->y - b->y;
    float sum_of_squares = powf(x_dist, 2) + powf(y_dist, 2);
    return sqrtf(sum_of_squares);
}

struct point *str_to_point(char *str) {
    struct point *the_point = (struct point*)malloc(sizeof(struct point));
    sscanf(str, "%f,%f", &(the_point->x), &(the_point->y));
    return the_point;
}

