#include <stdio.h>
#include <string.h>

char name[80];
void chomp(char *str);

int main() {
    printf("Enter your name: ");
    fgets(name, sizeof(name), stdin);

    /* gets a newline character too, so strip it */
    chomp(name);

    printf("You entered '%s'. ", name);
    printf("That must be your name!\n");
    return 0;
}

void chomp(char *str) {
    /* index of the last character
     * this means strlen - 1
     * since str is 0-indexed
     */
    int last = strlen(str) - 1;
    if (last > -1 && str[last] == '\n') {
        str[last] = '\0';
    }
}
