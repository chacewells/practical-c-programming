#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const double IN_TO_CM  = 2.54;
const double FT_TO_M   = 0.305;
const double MI_TO_KM  = 1.609;
const double OZ_TO_G   = 28.350;
const double LB_TO_KG  = 0.453;
const double QT_TO_L   = 0.946;
const double GAL_TO_L  = 3.785;

const char
/* English UOMs */
INCH[]       = "in",
FOOT[]       = "ft",
MILE[]       = "mi",
OUNCE[]      = "oz",
POUND[]      = "lb",
QUART[]      = "qt",
GALLON[]     = "gal",
/* Metric UOMS */
CENTIMETER[] = "cm",
METER[]      = "m",
KILOMETER[]  = "km",
GRAM[]       = "g",
KILOGRAM[]   = "kg",
LITER[]      = "l";

struct Conversion {
    double value;
    char uom[3];
};

char line[100];     /* user input */
double value_in;    /* value from user input */
char uom[100];          /* UOM from user input */
double result;      /* the converted value */

/* conversion ratio & UOM determinedd from uom_in 8*/
struct Conversion *conversion;

void determine(char *uom, struct Conversion *conversion);
double convert(double input, double factor);

void init() {
    conversion = (struct Conversion*) malloc(sizeof(struct Conversion));
}

int main() {
    init();
    printf("Enter the value and UOM: ");

    /* read and parse user input */
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%lf %s", &value_in, uom);

    printf("value: %lf\n", value_in);
    printf("UOM: %s\n", uom);

    determine(uom, conversion);
    printf("determined conversion: %lf\n", conversion->value);
    printf("determined new UOM: %s\n", conversion->uom);
    result = convert( value_in, conversion->value );
    printf("%lf %s\n", result, conversion->uom);

    return 0;
}

void determine(char *uom, struct Conversion *conversion) {
    if ( strcmp(uom, INCH) == 0 ) {
        conversion->value = IN_TO_CM;
        strcpy( conversion->uom, CENTIMETER );
    }
    else if ( strcmp(uom, FOOT) == 0 ) {
        conversion->value = FT_TO_M;
        strcpy( conversion->uom, METER );
    }
    else if ( strcmp(uom, MILE) == 0 ) {
        conversion->value = MI_TO_KM;
        strcpy( conversion->uom, KILOMETER );
    }
    else if ( strcmp(uom, OUNCE) == 0 ) {
        conversion->value = OZ_TO_G;
        strcpy( conversion->uom, GRAM );
    }
    else if ( strcmp(uom, POUND) == 0 ) {
        conversion->value = LB_TO_KG;
        strcpy( conversion->uom, KILOGRAM );
    }
    else if ( strcmp(uom, QUART) == 0 ) {
        conversion->value = QT_TO_L;
        strcpy( conversion->uom, LITER );
    }
    else if ( strcmp(uom, GALLON) == 0 ) {
        conversion->value = GAL_TO_L;
        strcpy( conversion->uom, LITER );
    }
    else {
        fprintf(stderr, "'%s' is not a valid UOM", uom);
        exit(1);
    }
}

double convert(double input, double factor) {
    double result = input * factor;
    return result;
}
