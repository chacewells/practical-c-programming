/* area.c
 * Aaron Wells
 * Computes the area of a 6.8x2.3 rectangle
 */
#include <stdio.h>

float compute_area(float w, float h);
float width, height, area;

int main() {
    /* initialize width and height */
    width = 6.8;
    height = 2.3;
    /* compute the area */
    area = compute_area(width, height);

    printf("area of a %.1f x %.1f rectangle = %.1f\n", width, height, area);
    return 0;
}

/* computes the area of a rectangle */
/* w - the width */
/* h - the height */
float compute_area(float w, float h) {
    return w * h;
}
