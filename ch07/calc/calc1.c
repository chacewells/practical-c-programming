#include <stdio.h>
char line[100]; /* user input */
int result;     /* result of calculations */
char operator;  /* operator specified */
int value;      /* value specified after the operator */

int main() {
    result = 0; /* initialize the result */

    /* Loop forever (or til we hit the break statement) */
    while (1) {
        printf("Result: %d\n", result);

        printf("Enter operator and number: ");
        fgets(line, sizeof(line), stdin);
        sscanf(line, "%c %d", &operator, &value);

        if (operator == 'q') {
            break;
        } else if (operator == '+') {
            result += value;
        } else if(operator == '-') {
            result -= value;
        } else if(operator == '*') {
            result *= value;
        } else if(operator == '/') {
            if (value == 0) {
                puts("Error: Divide by zero");
                puts("   operation ignored");
            } else {
                result /= value;
            }
            result /= value;
        } else {
            printf("Unknown operator %c\n", operator);
        }
    }
}
