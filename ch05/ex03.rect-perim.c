#include <stdio.h>

double rect_perimeter(double width, double height);

double width, height, perimeter;
char line[100];

int main() {
    printf("Enter the width and height (width height): ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%lf %lf", &width, &height);

    perimeter = rect_perimeter(width, height);

    printf("Perimeter of %.02lfx%.02lf: %.02lf\n", width, height, perimeter);

    return 0;
}

double rect_perimeter(double width, double height) {
    return 2 * (width + height);
}
