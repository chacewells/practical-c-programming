#include <stdio.h>

const int MINUTES_PER_HOUR = 60;

void minutes_to_hours_minutes(int minutes_in, int *hours_out, int *minutes_out);

int minutes_in;
int hours_out, minutes_out;

char line[100];

int main() {
    printf("Enter minutes: ");
    
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &minutes_in);

    minutes_to_hours_minutes(minutes_in, &hours_out, &minutes_out);

    printf("%d minutes is %d hours and %d minutes\n",
                minutes_in,
                hours_out,
                minutes_out);

    return 0;
}

void minutes_to_hours_minutes(int minutes_in, int *hours_out, int *minutes_out) {
    *hours_out = minutes_in / MINUTES_PER_HOUR;
    *minutes_out = minutes_in % MINUTES_PER_HOUR;
}
