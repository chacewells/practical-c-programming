#include <stdio.h>

#define X_SIZE 3
#define Y_SIZE 2

int array[X_SIZE][Y_SIZE];

int main() {
    int x;

    array[0][0] = 0 * 10 + 0;
    array[0][1] = 0 * 10 + 0;
    array[1][0] = 1 * 10 + 0;
    array[1][1] = 1 * 10 + 1;
    array[2][0] = 2 * 10 + 0;
    array[2][1] = 2 * 10 + 1;

    for (x = 0; x < (X_SIZE+1); ++x) {
        printf("array[%d]: [", x);
        printf("%d, %d]\n", array[x][0], array[x][1]);
    }
}
