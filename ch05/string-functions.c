#include <stdio.h>
#include <string.h>

char bill[5];
char bob[4];
char billbob[8];
char *bobbill; /* alias? */
int compresult;

/* prints bill, prints bob, then puts them together and prints the result */
int main() {
    strcpy(bill, "Bill");
    strcpy(bob, "Bob");
    printf("bill is \"%s\"; bob is \"%s\"\n", bill, bob);

    strcat(billbob, bill);
    strcat(billbob, bob);
    printf("together: \"%s\"\n", billbob);
    bobbill = billbob;
    bobbill[0] = 'K';
    printf("changed? \"%s\"\n", billbob);

    printf("the length of billbob is %lu\n", strlen(billbob));

    compresult = strcmp(bill, bob);
    printf("%s to %s: %d\n", bill, bob, compresult);
    compresult = strcmp(bob, bill);
    printf("%s to %s: %d\n", bob, bill, compresult);

    return 0;
}
