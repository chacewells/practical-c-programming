#include <stdio.h>
char line[100]; /* input from console */
int value;      /* a value to double */

int main() {
    printf("Enter a value: ");

    fgets(line, sizeof(line), stdin); /* get the line */
    sscanf(line, "%d", &value);       /* extract the number */

    printf("Twice %d is %d\n", value, value * 2);
    return 0;
}
