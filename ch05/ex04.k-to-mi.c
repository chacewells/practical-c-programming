#include <stdio.h>

const double KPH_TO_MPH = 0.6213712;

double kph, mph;
char line[100];

int main() {
    printf("Enter speed in KPH: ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%lf", &kph);
    
    mph = kph * KPH_TO_MPH;

    printf("%.2lf KPH = %.2lf MPH\n", kph, mph);
    return 0;
}
