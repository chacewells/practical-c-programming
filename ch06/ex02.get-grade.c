#include <stdio.h>
#include <stdlib.h>

/* user input */
char line[100];

/* percent score */
int percent_score;

char letter_grade;

void generate_letter_grade(int score, char *grade_ptr);

int main() {
    printf("Enter the grade: ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &percent_score);

    generate_letter_grade(percent_score, &letter_grade);
    if (letter_grade == '\0') {
        exit(1);
    }
    printf("letter grade: %c\n", letter_grade);

    return 0;
}

void generate_letter_grade(int score, char *grade_ptr) {
    if (score >= 0 && score <= 60)
    {
        *grade_ptr = 'F';
    }
    else if (score <= 70)
    {
        *grade_ptr = 'D';
    }
    else if (score <= 80)
    {
        *grade_ptr = 'C';
    }
    else if (score <= 90)
    {
        *grade_ptr = 'B';
    }
    else if (score <= 100)
    {
        *grade_ptr = 'A';
    }
    else
    {
        fprintf(
            stderr,
            "%d is not a valid grade. No score for you!\n",
            score);
        *grade_ptr = '\0';
    }
}
