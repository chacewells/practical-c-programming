#include <stdio.h>
#include <string.h>

/* determine whether a year is a leap year */
int is_leap_year(int year);

/* user input */
char line[100];

/* message indicating whether year is a leap year */
char message[100];

/* the year in question */
int year;

int main() {
    printf("Enter a year: ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &year);

    strcpy(message,
        is_leap_year(year)
        ? "%d is a leap year\n"
        : "%d is not a leap year\n");
    
    printf(message, year);

    return 0;
}

int is_leap_year(int year) {
    if ( (year % 100 == 0) // not a leap year if divisible by 100 and not by 400
     && (year % 400 != 0) ) {
        return 0;
     } else { // otherwise a leap year if divisible by 4
        return year % 4 == 0;
     }
}
