#include <stdio.h>

const int MINUTES_PER_HOUR = 60;

int hours_minutes_to_minutes(int hours, int minutes);

int hours_in, minutes_in;
int minutes_out;
char line[100];

int main() {
    printf("Enter hours and minutes (H:M): ");
    
    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d:%d", &hours_in, &minutes_in);

    minutes_out = hours_minutes_to_minutes(hours_in, minutes_in);

    printf("%d hours and %d minutes is %d minutes\n", hours_in, minutes_in, minutes_out);

    return 0;
}

int hours_minutes_to_minutes(int hours, int minutes) {
    int result;
    result = minutes;
    result += (hours * MINUTES_PER_HOUR);
    return result;
}
