#!/usr/bin/perl
# calc-proto.pl
# A calculator for operating on simple integers

use v5.18;
use List::Util qw(any);

=head1 CODE DESIGN
=item  Loop
=item    Read an operator and number
=item    Do the calculation
=item    Display the result
=item  End-Loop
=cut

=head1 AVAILABLE OPERATIONS
=item + n add n to the current output buffer
=item - n subtract n from the current output buffer
=item * n multiply n by the current output buffer
=item / n divide the output buffer by n
=cut

my $buffer = 0;
say "calcResult: $buffer";

while (1) {
    print "Enter operator and number: ";
    my $line = <STDIN>;
    $line =~ s/^\s+|\s+$//g;
    my($op, $n) = split /\s+/, $line;

    exit if $op eq 'q';
    unless ($n =~ /^\d+$/) {
        say "$n is not an integer";
        next;
    }
    unless (any { $op eq $_ } qw{ + - * / }) {
        say "$op is not a valid operator";
        next;
    }
    if ($n == 0 and $op eq '/') {
        say "divide by 0 error";
        next;
    }
    
    $buffer = eval "$buffer $op $n";
} continue {
    say "calcResult: $buffer";
}
