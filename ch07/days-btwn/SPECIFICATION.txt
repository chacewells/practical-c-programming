******************************
* DAYS BETWEEN SPECIFICATION *
******************************

Takes two dates in the format '%m/%d/%y' and prints the number of days between them.

1] year is an integer
2] month is an integer between 1 and 12
    a] if it is not, an error is thrown indicating the issue and program execution stops
    b] if the month is between 1 and 9, it may be preceded by a zero [0]
3] day is an integer between 1 and 31
    a] if it is not, an error is thrown indicating the issue and program execution stops
    b] if it is between 1 and 9, it may be preceded by a zero [0]
    c] the days of each month are as follows. Any day entered outside of the specified range will cause an error to be thrown and program execution to stop
        January: 31
        February: 29 if it is a leap year, 28 otherwise
        March: 31
        April: 30
        May: 31
        June: 30
        July: 31
        August: 31
        September: 30
        October: 31
        November: 30
        December: 31
4] "days between" means the number of days **AFTER** teh start date, so:
    a] 0, if the dates are the same
    b] 1, if the second date is the day after the first
    c]-1, if the second date is the day before the first
