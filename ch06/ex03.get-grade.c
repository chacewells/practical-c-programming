#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* user input */
char line[100];

/* percent score */
int percent_score;

char letter_grade[3];

void generate_letter_grade(int score, char *grade_ptr);

int main() {
    printf("Enter the grade: ");

    fgets(line, sizeof(line), stdin);
    sscanf(line, "%d", &percent_score);

    generate_letter_grade(percent_score, letter_grade);
    if (letter_grade[0] == '\0') {
        exit(1);
    }
    printf("letter grade: %s\n", letter_grade);

    return 0;
}

void generate_letter_grade(int score, char *grade_ptr) {
    if (score >= 0 && score <= 60)
    {
        strcpy(grade_ptr, "F");
    }
    else if (score <= 70)
    {
        strcpy(grade_ptr, "D");
    }
    else if (score <= 80)
    {
        strcpy(grade_ptr, "C");
    }
    else if (score <= 90)
    {
        strcpy(grade_ptr, "B");
    }
    else if (score <= 100)
    {
        strcpy(grade_ptr, "A");
    }
    else
    {
        fprintf(
            stderr,
            "%d is not a valid grade. No score for you!\n",
            score);
        *grade_ptr = '\0';
        return;
    }

    if (score[0] != 'F') {
        /* insert modifier */
        int ones_place = score % 10;
        if ( (ones_place >= 1) && (ones_place <= 3) ) {
            strcat(grade_ptr, "-");
        }
        if ( (ones_place == 8)
          || (ones_place == 9)
          || (ones_place == 0) ) {
            strcat(grade_ptr, "+");
        }
    }
}
